# TUX

**Tux pictures!**

A large archive with various motifs (unfortunately I don't know the creator) as well as "self-created" ones with stable diffusion.

### Do it yourself!

* Install [Easy Diffusion](https://github.com/easydiffusion/) or any other stable diffusion software
* Models which works very well for anime style: https://civitai.com/models/7371/rev-animated
* Most important: https://civitai.com/models/124753/tuxiffusion

Example settings:

> Model: Rev-animated

> Sampler: DMP++ 2m (Karras)

> LoRA: Tuxifusion 0.5 / 1.0

> Prompt: ((masterpiece)),((best quality)),(detailed),one baby penguin wears a colored hoodie,monochrome image background,full body view,looking at camera,

> Negative prompt: blurry,bad quality,copyright,watermark,shadows,ears,bobble,cape,cloak,

<br>

**Credits**

Most of the Tux models are based on [Rev Animated - CC BY-NC-ND 4.0](https://civitai.com/models/7371/rev-animated) and [Tuxiffuion - public domain?/none](https://civitai.com/models/124753?modelVersionId=136245). Thanks a lot for models and lora!

